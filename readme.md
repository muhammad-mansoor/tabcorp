TabCorp JavaScript Developer Test

Instructions:

Implement a data structure that stores information as key/value pair and delete/remove data that is not updated or retrieved in the last 6 hours

Solution:

Solution consist of JS Prototype that contains four functions and one array(datastore):

get function:
  - Takes key as parameter
  - Check for the provided key within the Datastore
  - Update the `updatedOn` property on Match
  - Return value

set function:
  - Takes key and value as parameters
  - Check for the provided key within the Datastore
  - Prompt user on Match
  - Create a new object

updated function:
  - Takes key and value as parameters
  - Check for the provided key within the Datastore
  - Create a new object on Match

remove function:
  - Stores the updatedOn or createdOn property of objects in an Array
  - Check if the value is older than 3 seconds
  - Update the main Datastore on Match
