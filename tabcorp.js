'use strict';
// Main Prototype
// It is based on the assumption that you cannot set 'null' as value of your key/value Datastore
function TabCorp() {


    this.ds = [];

    this.get = function(key) {
        var self = this;
        var hasVal = false,
            result = null;

        for (var i = 0, len = self.ds.length; i < len; i++) {
            if (self.ds[i].key === key) {
                hasVal = true;
                result = self.ds[i].value;
                self.ds[i].updatedOn = new Date();
            }
        }
        if (hasVal === true) {
            return result;
        } else {
            return result;
        }
    };

    this.set = function(key, value) {
        var self = this;
        var result = self.get(key) ? alert("key already exist") : self.ds.push({
            key: key,
            value: value,
            createdOn: new Date()
        });
    };

    this.update = function(key, value) {
        var self = this;
        var result = self.get(key);

        if (result) {
            for (var i = 0, len = self.ds.length; i < len; i++) {
                if (self.ds[i].key === key) {
                    self.ds[i].value = value;
                    self.ds[i].updatedOn = new Date();
                }
            }
        } else {
            alert("Key doesnot exist");
        }

    };

    this.remove = function() {
        var self = this;
        var trash = [];
        for (var i = 0, len = self.ds.length; i < len; i++) {
            var obj = self.ds[i];
            obj.updatedOn ? trash.push(obj.updatedOn) : trash.push(obj.createdOn);
        }
        console.log(trash);
        var now = new Date();
        for (var i = 0, len = trash.length; i < len; i++) {
            var diff = now - trash[i];
            if (diff > 3000) {
                self.ds.splice(i, 1)

            }
        }
        console.log(self.ds);


    }


}

var tab = new TabCorp();
tab.set("b", 1);
tab.set("c", 3);

// One Time Execution after 6 seconds
setTimeout(function() {
    console.log("setTimeout");
    tab.set("d", 6);
    tab.update("b", 4);
    tab.remove();

}, 6000);

// Continuous Execution of 7 seconds
setInterval(function() {
  console.log("setInterval");
  tab.remove();
}, 7000);
